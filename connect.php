<?php
	$name = $_POST['name'];
	$email = $_POST['email'];
	$age = $_POST['age'];
	$role = $_POST['role'];
	$radio = $_POST['radio'];
	$checkbox = $_POST['checkbox'];
    $comments = $_POST['comments'];

	// Database connection
	$conn = new mysqli('localhost','root','','studentfeedback');
	if($conn->connect_error){
		echo "$conn->connect_error";
		die("Connection Failed : ". $conn->connect_error);
	} else {
		$stmt = $conn->prepare("insert into test(name, email, age, role, radio, checkbox, comments) values(?, ?, ?, ?, ?, ?, ?)");
		$stmt->bind_param("sssssss", $name, $email, $age, $role, $radio, $checkbox, $comments);
		$execval = $stmt->execute();
		echo $execval;
		echo "Submitted successfully...";
		$stmt->close();
		$conn->close();
	}
?>